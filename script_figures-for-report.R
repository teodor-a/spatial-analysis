#### Load libraries ####
#library(affy)
library(standR)
library(tidyverse)
library(ggtext)
library(ComplexHeatmap)

#### Load data ####
load('IntermediateData/spe_batchC.RData')

# Define which assays (normalization methods) we want to highlight:
methods <-
  c('logcounts', # Counts per million transformed
    'scaled_geomMean_nuc',# scaled to nuclei count
    'cyclic_loess', # cyclic loess on log2 transformed
    'CPM_RUV4', # RUV4 on CPM
    'CL_RUV4_seg' # RUV4 on cyclic loess
    )

methods_human <- c('Raw', 'Scaled to nuc.', 'CL', 'RUV4', 'CL+RUV4')

methods_df <- data.frame(coded = methods, human = methods_human)

basesiz <- 15


#### Define functions ####
## Function to return ggplot showing density of expressions:
plotDens <- function(expmat, x) {
  df <-
    expmat %>%
    as.data.frame() %>%
    rownames_to_column(var = 'Probe') %>%
    tidyr::pivot_longer(cols = starts_with('DSP-'),
                        names_to = 'Sample',
                        values_to = 'Expression')
  
  plt <-
    ggplot2::ggplot(data = df, mapping = aes(x = Expression, color = Sample)) +
    geom_density() +
    xlim(0, 12) +
    ylab('Frequency') +
    theme_classic(base_size = basesiz) +
    theme(legend.position = 'none', axis.title.y = element_text(size = 20))
  
  return(plt)
}

## Function for selecting matrix, marker and genes
get_mat_markers_genes <- function(spe, assay_name, probelist, gene_names = NULL, cluster.res = NULL) {
  
  CTA.mat <- assay(spe, assay_name) 
  
  CTA.mat <- CTA.mat %>%
    as.data.frame() %>%
    filter(rownames(.) %in% probelist) %>%
    t() %>%
    as.data.frame()
  
  markers <- CTA.mat %>%
    rownames_to_column(var = 'SampleID') %>%
    mutate(SampleID = str_remove(SampleID, pattern = '.dcc')) %>%
    left_join(as.data.frame(colData(spe)), by = "SampleID") %>%
    select(SampleID, dcc, segment) %>%
    pull(segment)
  
  return(list("exp.m" = CTA.mat[probelist],
              "markers" = markers))
}

### Raw pca plots for presentation ####
reduction_CPM <- stats::prcomp(assay(spe, 'logcounts'))$rotation

pca1 <-
  standR::drawPCA(spe,
                  #assay = 'logcounts',
                  col = slide_short,
                  precomputed = reduction_CPM) +
  theme_classic(base_size = basesiz) +
  theme(legend.title = element_blank(),
        legend.key.size = unit(2, 'line'),
        legend.text = element_text(size = 22),
        legend.key = element_rect(),
        legend.position = 'top') +
  guides(color = guide_legend(override.aes = list(size = 10)),
         shape = guide_legend(override.aes = list(size = 10)))

ggsave(plot = pca1, filename = 'PCA_slide.png', device = 'png',
       width = 20, height = 10, units = 'cm')

pca2 <-
  standR::drawPCA(spe,
                  assay = 'logcounts',
                  col = segment_name) +
  theme_classic(base_size = basesiz) +
  theme(legend.title = element_blank(),
        legend.key.size = unit(2, 'line'),
        legend.text = element_text(size = 22),
        legend.key = element_rect(),
        legend.position = 'top') +
  guides(color = guide_legend(override.aes = list(size = 10)),
         shape = guide_legend(override.aes = list(size = 10)))

ggsave(plot = pca2, filename = 'PCA_segment.png', device = 'png',
       width = 20, height = 10, units = 'cm')

pca3 <-
  standR::drawPCA(spe,
                  assay = 'logcounts',
                  col = timepoint_simplest) +
  theme_classic(base_size = basesiz) +
  theme(legend.title = element_blank(),
        legend.key.size = unit(2, 'line'),
        legend.text = element_text(size = 22),
        legend.key = element_rect(),
        legend.position = 'top') +
  guides(color = guide_legend(override.aes = list(size = 10)),
         shape = guide_legend(override.aes = list(size = 10)))

ggsave(plot = pca3, filename = 'PCA_time.png', device = 'png',
       width = 20, height = 10, units = 'cm')



#### Dist, RLE, PCA for the different norm. methods ####

nameplots <- list(empty_plot = ggplot(data = tibble(x = 1, y = 1)) +
                    geom_blank() +
                    theme_classic() +
                    theme(axis.line = element_blank()))

distplots <- list(dist_title = ggplot(data = tibble(x = 1, y = 0.1,
                                                    label = 'Distributions')) +
                    aes(x = x, y = y, label = label) +
                    geom_textbox(box.color = NA, fill = NA, size = 10, halign = 0.5) +
                    scale_x_continuous(expand = c(0, 0)) +
                    scale_y_continuous(expand = c(0, 0)) +
                    theme_void() +
                    theme(plot.margin = margin(0, 0, 0, 0))
                  )

rleplots <- list(dist_title = ggplot(data = tibble(x = 1, y = 0.1,
                                                   label = 'RLE')) +
                   aes(x = x, y = y, label = label) +
                   geom_textbox(box.color = NA, fill = NA, size = 10, halign = 0.5) +
                   scale_x_continuous(expand = c(0, 0)) +
                   scale_y_continuous(expand = c(0, 0)) +
                   theme_void() +
                   theme(plot.margin = margin(0, 0, 0, 0)))

pcaplots <- list(dist_title = ggplot(data = tibble(x = 1, y = 0.1,
                                                   label = 'PCA')) +
                   aes(x = x, y = y, label = label) +
                   geom_textbox(box.color = NA, fill = NA, size = 10, halign = 0.5) +
                   scale_x_continuous(expand = c(0, 0)) +
                   scale_y_continuous(expand = c(0, 0)) +
                   theme_void() +
                   theme(plot.margin = margin(0, 0, 0, 0)))

for(method in methods) {
  ### Distribution plots ###
  title <- paste('Density', method, sep = '_')
  dist <- plotDens(assay(spe, method), method) # Defined above
  # Add the plot to the list:
  distplots[[title]] <- dist
  
  ### RLE plots ###
  title <- paste('RLE', method, sep = '_')
  rle <- standR::plotRLExpr(spe, assay = method, color = slide_short) +
    xlab('Segment') +
    theme_classic(base_size = basesiz) +
    theme(legend.position = 'none', axis.title.y = element_text(size = 20))
  # Add the plot to the list:
  rleplots[[title]] <- rle
  
  ### PCA plots ###
  # Make an informative title (only used in plot list):
  title <- paste('PCA', method, sep = '_')
  # Fetch the assay index in spe that contains the assay for current method:
  asind <- which(names(assays(spe)) == method)
  # Draw the PCA plot:
  pca <- standR::drawPCA(spe,
                         assay = asind, # Index needed
                         col = slide_short,
                         shape = segment) +
    theme_classic(base_size = basesiz) +
    theme(legend.title = element_blank(),
          legend.key.size = unit(2, 'line'),
          legend.text = element_text(size = 22),
          legend.key = element_rect()) +
    guides(color = guide_legend(override.aes = list(size = 10)),
           shape = guide_legend(override.aes = list(size = 10)))
  # Add the plot to the list:
  pcaplots[[title]] <- pca
  
  # Make plot to hold name for row (the method the row represents):
  title <- paste('name', method, sep = '_')
  nameplot <- ggplot(data = methods_df[methods_df$coded == method,],
                     mapping =  aes(x = 0, y = 1, label = human)) +
    geom_textbox(box.color = NA, fill = NA, size = 10, halign = 1, hjust = 1) +
    scale_x_continuous(limits = c(-1, 0), expand = c(0, 0)) +
    scale_y_continuous(expand = c(0, 0)) +
    theme_void() +
    theme(plot.margin = margin(0, 0, 0, 0))
  # Add the plot to the list:
  nameplots[[title]] <- nameplot
  
  
}



# Need patchwork 1.2.x:
#url <- 'https://cloud.r-project.org/src/contrib/patchwork_1.2.0.tar.gz'
#install.packages(url, repos = NULL, type = 'source')
unloadNamespace('standR')
library(patchwork)

plots1 <-
  patchwork::wrap_plots(c(nameplots, distplots, rleplots, pcaplots),
                        ncol = 4, nrow = 6, byrow = F) +
  plot_layout(axes = "collect", guides = 'collect', widths = c(1,1,1,1))

ggsave(plot = plots1, filename = 'Chosen_dist_RLE_PCA.png', device = 'png',
       width = 42, height = 31, units = 'cm')


#### Heatmaps and Boxplots ####
probelist <- c('CD163', 'CD68', 'MRC1', # Macrophage
               'PRF1', 'CD8A', 'CD8B', # Cytotox T cell
               'NKG7', # Natural Killer Cell
               'GZMK', # Cytotox T / NK
               'CD3D', 'CD3E', 'CD3G', # T cells general
               'CD4' # T helper/Monocytes
               )


boxplotlist <- list()
heatmaplist <- NULL
for(method in methods) {
  
  mat.res <- get_mat_markers_genes(spe, method, probelist)
  
  centered_mat <- scale(mat.res[[1]], center = TRUE, scale = TRUE) # Center and scale the expression matrix
  #centered_mat <- mat.res[[1]]
  nameT <-  methods_df$human[methods_df$coded == method]
  

  heatmaplist <-
    heatmaplist + ComplexHeatmap::Heatmap(centered_mat,
                                            name = nameT,
                                            row_split = mat.res[[2]],
                                            show_row_names = F,
                                            cluster_columns = F,
                                            cluster_rows = F,
                                          column_title = nameT,
                                            heatmap_legend_param = 
                                              list(direction = 'horizontal'),
                                            border = T,
                                            border_gp = gpar(col = "black"))
  
  
  
  centered_mat_long <- centered_mat %>%
    as.data.frame() %>%
    mutate(markers = mat.res[[2]]) %>%
    gather(key = "variable", value = "value", -markers) 
  
  boxplotlist[[method]] <- centered_mat_long %>%
    ggplot(aes(x = markers, y = value, fill = markers)) +
    geom_boxplot() + 
    theme_bw(base_size = basesiz) +
    theme(axis.text.x = element_blank(),
          axis.title.x = element_blank(),
          axis.title.y = element_text(size = 20),
          legend.title = element_blank(),
          legend.key.size = unit(2, 'line'),
          legend.text = element_text(size = 22),
          legend.direction = 'horizontal') + 
    guides(color = guide_legend(override.aes = list(size = 10)),
           shape = guide_legend(override.aes = list(size = 10))) +
    facet_wrap(~ variable,
               nrow = 1,
               scales = "free") +
    ylab(methods_df$human[methods_df$coded == method])
  
  
  
}


tiff(filename = 'Chosen_heatmaps.tif',
    width = 1000, height = 570, units = 'px', res = '100', pointsize = 20)
draw(heatmaplist,
     #merge_legends = TRUE,
     #show_heatmap_legend = T,
     align_heatmap_legend = 'heatmap_center',
     heatmap_legend_side = 'bottom',
     #legend_grouping = 'adjusted',
     column_title_side = 'top',
     column_title_gp = gpar(fontsize = 16))
dev.off()

plots2 <- patchwork::wrap_plots(boxplotlist, ncol = 1) +
  plot_layout(guides = 'collect', axes = 'collect', axis_titles = 'collect') +
  guide_area()

  
ggsave(plot = plots2, filename = 'Chosen_boxplots.png', device = 'png',
       width = 30, height = 26, units = 'cm', scale = 1)


#### Some markers ####
# Read a list of selected markers from file:
selected_markers <- scan(file = 'selected_markers.txt', what = character())

expr <- t(assay(spe, 'logcounts'))[,selected_markers]
meta <-as.data.frame(colData(spe))
sum(rownames(expr) != rownames(meta)) # Should be 0

df <- cbind(expr, meta) %>%
  select(all_of(c('timepoint_numeric',
                  'timepoint_simple',
                  selected_markers,
                  'patient_id', 
                  'segment_name'))) %>%
  mutate(timepoint_numeric = as.factor(timepoint_numeric)) %>%
  mutate(tm2 = case_when(timepoint_numeric == 9 | timepoint_numeric == 12 ~ '9 + 12',
                         .default = timepoint_numeric)) %>%
  pivot_longer(cols = all_of(c(selected_markers)),
               names_to = 'Target', values_to = 'Expression')


library(stringr)
plot.new()
markerplotlist <- list()
for(patient in unique(df$patient_id)) {
  
  
  plotdata <- droplevels(df[df$patient_id == patient,])
  patient_nr <- str_extract(string = unique(plotdata$patient_id),
                            pattern = '[1-9]+')
  
  markerplotlist[[patient]] <-
    ggplot(data = plotdata,
           mapping = aes(x = tm2, y = Expression, fill = Target)) +
    geom_boxplot() +
    xlab(paste('Patient ',
               patient_nr)) +
  #  xlab('Timepoint') +
    title(patient) +
    theme_bw(base_size = 20) +
    theme(legend.key.size = unit(3, 'line'),
          legend.text = element_text(size = 20)) +
    facet_wrap(~segment_name)

}

design <- "
  12
  33
"

plots3 <-
  patchwork::wrap_plots(markerplotlist[c(1,2)]) +
     guide_area() +
  plot_layout(ncol = 2,
              axis_titles = 'collect',
              guides = 'collect',
              design = design,
              heights = c(9,1)) &
  theme(legend.direction = 'horizontal')


ggsave(plot = plots3, filename = 'Chosen_markers.png', device = 'png',
       width = 30, height = 15, units = 'cm')



#### Even more markers ####
more_markers <- c('ANXA1',
                  #'MAP3K5',
                  'LFNG')

expr <- t(assay(spe, 'logcounts'))[,more_markers]
meta <-as.data.frame(colData(spe))
sum(rownames(expr) != rownames(meta)) # Should be 0

df <- cbind(expr, meta) %>%
  select(all_of(c('timepoint_numeric',
                  'timepoint_simple',
                  more_markers,
                  'patient_id', 
                  'segment_name'))) %>%
  mutate(timepoint_numeric = as.factor(timepoint_numeric)) %>%
  mutate(tm2 = case_when(timepoint_numeric == 9 | timepoint_numeric == 12 ~ '9 + 12',
                         .default = timepoint_numeric)) %>%
  pivot_longer(cols = all_of(c(more_markers)),
               names_to = 'Target', values_to = 'Expression')

markerplotlist <- list()
for(patient in unique(df$patient_id)) {
  
  
  plotdata <- droplevels(df[df$patient_id == patient,])
  patient_nr <- str_extract(string = unique(plotdata$patient_id),
                            pattern = '[1-9]+')
  
  markerplotlist[[patient]] <-
    ggplot(data = plotdata,
           mapping = aes(x = tm2, y = Expression, fill = Target)) +
    geom_boxplot() +
    xlab(paste('Patient ',
               patient_nr)) +
    #  xlab('Timepoint') +
    title(patient) +
    theme_bw(base_size = 20) +
    theme(legend.key.size = unit(3, 'line'),
          legend.text = element_text(size = 20)) +
    facet_wrap(~segment_name)
  
  ggsave(plot = markerplotlist[[patient]],
         filename = paste0('moremarkers_', patient, '.png'),
         device = 'png', scale = 1, width = 20, height = 10)
  
}
