#~~~~~~~~~~~~~~~~~~~~~~~~~~
###### Libraries used #####
#~~~~~~~~~~~~~~~~~~~~~~~~~~
library(GeomxTools) # For working with GeoMX data
library(openxlsx) # For reading and writing excel files
library(tidyverse) # Manipulating dataframes
library(ggplot2) # Plotting
library(ggforce) # Extra ggplot funcitonality
library(SpatialExperiment) # OBS Move this line to top of script.
library(standR) # OBS
library(ggrepel) # OBS move this line to top.



if(!dir.exists('IntermediateData')) {
  dir.create('IntermediateData')
}


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
###### Annotation files preprocessing #####
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The annotation files require some preprocessing,
# including pasting together followed by writing to new excel file.

# Path to file containing annotation data:
annotpath <- file.path('Data', 'annotation_file_P2301.xlsx')

# Path to additional file containing more segment metadata:
segsumpath <- file.path('Data', 'segmentSummary.xlsx')

# Load the annotation excel file into memory: 
annot <- openxlsx::read.xlsx(annotpath, sep.names = '_')

# Tidy column names:
colnames(annot) <-
  str_replace_all(colnames(annot), pattern = ' ', replacement = '_') %>%
  tolower()

# Tidy ROI column:
annot$roi <- str_replace_all(annot$roi, pattern = '[^a-z0-9]', replacement = '')

# Make unique identifier column used later for binding dataframes:
annot$bindcol <- paste(annot$slide_name, annot$roi, annot$segment, sep = '_')

# Load the segment summaries excel file into memory:
segsum <- read.xlsx(segsumpath, sep.names = '_')

# Tidy column names:
colnames(segsum) <- 
  str_replace_all(colnames(segsum), pattern = ' ', replacement = '_') %>%
  tolower() %>%
  str_replace_all(pattern = 'nuclei_count',
                  replacement = 'nuclei')

# Make unique identifier column used later for binding dataframes:
segsum$bindcol <-
  paste(segsum$slide_name, segsum$roi_name, segsum$segment_name, sep = '_')

# Combine the two dataframes containing segment data:
annotcombined <-
  left_join(x = annot,
            y = segsum,
            by = c('bindcol', 'scan_name', 'slide_name'),
            keep = F) %>%
  # Make new column containing short slide name:
  mutate(slide_short = str_extract(slide_name, 'Slide[0-9]')) %>%
  mutate(slide_short = tidyr::replace_na(slide_short, 'ctrl'),
         segment_name = tidyr::replace_na(segment_name, 'ctrl')) %>%
  mutate(bindcol = NULL,
         area = as.numeric(area))

# Change column name slide_name back to 'slide name' because 
# readNanoStringGeoMxSet() requires it.
colnames(annotcombined)[colnames(annotcombined) == 'slide_name'] <- 'slide name'

# Save resulting annotation dataframe as an excel file because
# readNanoStringGeoMxSet() requires reading from excel file only...
write.xlsx(annotcombined, file = 'Data/annotcombined.xlsx', asTable = T)

#~~~~~~~~~~~~~~~~~~
####Load data #####
#~~~~~~~~~~~~~~~~~~
# Get paths to DCC files:
DCCFiles <- list.files(path = 'Data',
                       pattern = '.dcc$', full.names = T)

# Get path to PKCFiles:
PKCFiles <- list.files(path = 'Data',
                       pattern = '.pkc$', full.names = T)

# Get path to the annotFile prepared above:
annotFile <- list.files(path = 'Data',
                        pattern = 'annotcombined', full.names = T)

# Create nanostring GeoMx data object by reading files from disk:
data <-
    readNanoStringGeoMxSet(dccFiles = DCCFiles,
                           pkcFiles = PKCFiles,
                           phenoDataFile = annotFile,
                           phenoDataSheet = "Sheet 1",
                           phenoDataDccColName = "sample_id",
                           protocolDataColNames = c("aoi", "roi", 'segment'),
                           experimentDataColNames = c("panel"),
                           analyte = 'RNA')

# Create dataframes which will be used to track when and why a segment
# or probe is filtered out:
filtertable_segment <- sData(data) %>%
  select(slide_short, roi_name, segment_name) %>%
  mutate(Reason = NA)
filtertable_probe <- data.frame(row.names = unique(fData(data)$TargetName)) %>%
  mutate(Reason = NA)

#~~~~~~~~~~~~~~~~~~~~~~
#### Explore data #####
#~~~~~~~~~~~~~~~~~~~~~~

# The complete meatadata from annotcombined is in data@phenoData
# access by data@phenoData$segment_name
# or just data$segment_name
# The actual expression matrix can be found in:
assayDataElement(data, elt = 'exprs') %>%
  head()

## We want to plot a sankey diagram to get an overview of our raw data.
# To do this, first create a matrix containing counts of our samples:
count_mat <- dplyr::count(pData(data), slide_short, segment_name)

# "Gather" the data:
test_gr <- gather_set_data(count_mat, 1:2)
test_gr[is.na(test_gr)] <- 'C'

# Then create the plot using geom_parallel_sets in a ggplot:
ggplot(test_gr, aes(x, id = id, split = y, value = n)) +
  geom_parallel_sets(aes(fill = segment_name), alpha = 0.5, axis.width = 0.1) +
  geom_parallel_sets_axes(axis.width = 0.2) +
  geom_parallel_sets_labels(color = "white", size = 5) +
  theme_classic(base_size = 17) + 
  theme(legend.position = "bottom",
        axis.ticks.y = element_blank(),
        axis.line = element_blank(),
        axis.text.y = element_blank()) +
  scale_y_continuous(expand = expansion(0)) + 
  scale_x_discrete(expand = expansion(0)) +
  labs(x = "", y = "") +
  annotate(geom = "text", x = 2.13, y = 20, angle = -90, size = 5,
           label = paste(sum(test_gr$n)/2,'segments')) +
  annotate(geom = "segment",
           x = 2.15, xend = 2.15,
           y = 0, yend = 69,
           lwd = 2)
# Save it:
ggsave(filename = 'Sankey_start.png', path = 'QC', device = 'png',
       width = 20, height = 12, units = 'cm')

# Shift all counts by one to prevent NAs being created when we log later:
data <- shiftCountsOne(data, useDALogic = TRUE)

# Save the raw data object to disk:
data_raw <- data
save(file = 'IntermediateData/Data_raw.RData', data_raw)


#~~~~~~~~~~~~~~~~~~~~~~
#### SEGMENT QC #######
#~~~~~~~~~~~~~~~~~~~~~~
## In this section we investigate which segments do not reach standards.

# Define the thresholds for what makes a good segment/probe:
QC_params <-
  list(minSegmentReads = 1000, # Minimum number of reads (1000)
       percentTrimmed = 80,    # Minimum % of reads trimmed (80%)
       percentStitched = 80,   # Minimum % of reads stitched (80%)
       percentAligned = 75,    # Minimum % of reads aligned (75%)
       percentSaturation = 50,
       # Minimum sequencing saturation (50%), can be high as 70%.
       minNegativeCount = 1,   # Minimum negative control counts (1)
       maxNTCCount = 1000,     # Maximum counts observed in NTC well (9000)
       minNuclei = 10, # Consider using 20 after all.
       # Minimum # of nuclei estimated (20),
       # we know that many segments have a low count
       minArea = 100)
      # Minimum segment area (1000),
      # setting quite low. Though, again, we have small areas defined

# Set the QC flags based on above thresholds:
data <-
  GeomxTools::setSegmentQCFlags(data, qcCutoffs = QC_params)        

# Collate QC Results:
QCResults <- protocolData(data)[["QCFlags"]]

# Set NA to False:
QCResults[is.na(QCResults)] <- F

# Create a QC summary dataframe:
flag_columns <- colnames(QCResults)
QC_Summary <- data.frame(Pass = colSums(!QCResults[, flag_columns]),
                         Warning = colSums(QCResults[, flag_columns]))

# Make a column that summarises all the checks into a single column:
# (If all flags are FALSE, the sample gets a pass.
# If any flag is TRUE, the sample gets a warning.)
QCResults$QCStatus <- apply(QCResults, 1L, function(x) {
  ifelse(sum(x) == 0L, "PASS", "WARNING")
})

# Make a column that counts the number of flags raised:
QC_Summary["TOTAL FLAGS", ] <-
  c(sum(QCResults[, "QCStatus"] == "PASS"),
    sum(QCResults[, "QCStatus"] == "WARNING"))

col_by <- "segment_name"

# Function to create histogram summary of  a given QC statistic:
QC_histogram <- function(assay_data = NULL,
                         annotation = NULL,
                         fill_by = NULL,
                         thr = NULL,
                         scale_trans = NULL) {
  plt <- ggplot(assay_data,
                aes_string(x = paste0("unlist(`", annotation, "`)"),
# OBS! aes_string is deprecated.
#See https://ggplot2.tidyverse.org/reference/aes_.html
                           fill = fill_by)) +
    geom_histogram(bins = 50) +
    geom_vline(xintercept = thr, lty = "dashed", color = "black") +
    theme_bw() + guides(fill = "none") +
    facet_wrap(as.formula(paste("~", fill_by)), nrow = 4) +
    labs(x = annotation, y = "Segments, #", title = annotation)
  if(!is.null(scale_trans)) {
    plt <- plt +
      scale_x_continuous(trans = scale_trans)
  }
  plt
}

# Initialise list to save histograms in:
qcplots <- list()
# Make histogram of Trimmed %:
qcplots$trimh <-
  QC_histogram(sData(data), "Trimmed (%)",
               col_by, QC_params[['percentTrimmed']])
# Make histogram of Stitched %:
qcplots$stith <-
  QC_histogram(sData(data), "Stitched (%)",
               col_by, QC_params[['percentStitched']])
# Make histogram of Aligned %:
qcplots$aligh <-
  QC_histogram(sData(data), "Aligned (%)",
               col_by, QC_params[['percentAligned']])
# Make histogram of Saturated %:
qcplots$satuh <-
  QC_histogram(sData(data), "Saturated (%)",
               col_by, QC_params[['percentSaturation']])
# Make histogram of area:
qcplots$areah1 <-
  QC_histogram(sData(data), "area",
               col_by, QC_params[['minArea']], scale_trans = "log10")
# Make histogram of area, with line at 5000 instead of at the defined threshold:
qcplots$areah2 <-
  QC_histogram(sData(data), "area",
               col_by, 5000, scale_trans = "log10")
# Make histogram of nuclei count:
qcplots$nuclh <-
  QC_histogram(sData(data), annotation = "nuclei",
               col_by, QC_params[['minNuclei']])

# Save all histograms to disk:
for(plot in names(qcplots)) {
  ggsave(plot = qcplots[[plot]], filename = paste0('QC_histo_', plot, '.png'),
         path = 'QC', device = 'png', width = 20, height = 12, units = 'cm')
  }

# These values will be used below:
pkcs <- annotation(data)
modules <- gsub(".pkc", "", pkcs)

# calculate the negative geometric means for each module
negativeGeoMeans <- 
  esBy(negativeControlSubset(data), 
       GROUP = "Module", 
       FUN = function(x) { 
         assayDataApply(x, MARGIN = 2, FUN = ngeoMean, elt = "exprs") 
       }) 

# Add the newly calculated values to the object:
protocolData(data)[["NegGeoMean"]] <- negativeGeoMeans

# explicitly copy the Negative geoMeans from sData to pData
negCols <- paste0("NegGeoMean_", modules)
pData(data)[, negCols] <- sData(data)[["NegGeoMean"]]

# Plot negative geometric mean for each module:
for(ann in negCols) {
  plt <- QC_histogram(assay_data = pData(data),
                      annotation = ann,
                      fill_by = col_by,
                      2, scale_trans = "log10")
  ggsave(plot = plt, filename = paste0(ann, '.png'), path = 'QC',
         device = 'png', width = 20, height = 12, units = 'cm')
}

# Detatch neg_geomean columns ahead of aggregateCounts call
pData(data) <- pData(data)[, !colnames(pData(data)) %in% negCols]

# Create and write to excel a summary of the thresholds used and the resulting
# pass/warning flags for each segment:
QCResults <-
  cbind(QCResults, sData(data)[,c('slide_short', 'segment_name', 'roi_name')])
openxlsx::write.xlsx(x = list(Flags = QCResults,
                              Params = as.data.frame(unlist(QC_params))),
                     file = 'QC/QCResults_and_params.xlsx', asTable = T)


#### The following line filters away samples who do not PASS all flags. ####
## If we have area threshold = 1000, this will filter 3 samples (segments)
## It is now 100, and all samples except for one are retained.
# We have been very generous with minArea and minNucleicount.
# Filter:
data <- data[, QCResults$QCStatus == 'PASS']
# Update tables tracking segments/probes:
filtertable_probe$Keep_after_segmentQC <-
  rownames(filtertable_probe) %in% unique(fData(data)$TargetName)
filtertable_segment$Keep_after_segmentQC <-
  rownames(filtertable_segment) %in% rownames(sData(data))

# Get list of removed samples:
removed <- QCResults[QCResults$QCStatus == 'WARNING',]

# Update filter tables. For each of the removed samples:
for(i in rownames(removed)) {
   # If the removed sample is not already mentioned in the filter tables:
  if(is.na(filtertable_segment$Reason[rownames(filtertable_segment) == i])) {
      # Then add a note on why it was removed:
    filtertable_segment$Reason[rownames(filtertable_segment) == i] <-
      paste('SegmentQC',
            paste(colnames(removed[,removed == T]), collapse = ','),
            sep = ':')
  }
}

# Subsetting our dataset has removed samples which did not pass QC
dim(data)


###### Probe QC ########
# Now we remove probes (which target transcripts) that are low quality.
# The two main QC parameters below are:
# minProbeRatio: the ratio of the probe's expression in all segments
# divided by the mean expression of all probes for the same target gene.
# percentFailGrubbs: fraction of segments in which the probe fails Grubb's test
# (which tests for outliers).

# First set the thresholds:
data <-
  setBioProbeQCFlags(data,
                     qcCutoffs = list(minProbeRatio = 0.1, # default 0.1
                                      percentFailGrubbs = 20 # default 20
                                      ),
                     removeLocalOutliers = TRUE)
# local outlier = probe problematic but not in all segments.

# Add flags based on the defined thresholds:
ProbeQCResults <- fData(data)[["QCFlags"]]

# Make table with QC result for each probe:
qc_df <- data.frame(Passed = sum(rowSums(ProbeQCResults[, -1]) == 0),
                    Global = sum(ProbeQCResults$GlobalGrubbsOutlier),
                    Local = sum(rowSums(ProbeQCResults[, -2:-1]) > 0
                                & !ProbeQCResults$GlobalGrubbsOutlier))
print(qc_df)



# Subset object to exclude all features that did not pass Ratio & Global testing
ProbeQCPassed <- 
  subset(data, 
         fData(data)[["QCFlags"]][,c("LowProbeRatio")] == FALSE &
           fData(data)[["QCFlags"]][,c("GlobalGrubbsOutlier")] == FALSE)
dim(ProbeQCPassed)
length(unique(featureData(ProbeQCPassed)[["TargetName"]]))
dim(data)
length(unique(featureData(data)[["TargetName"]]))


#### The following line filters away probes ####
### who do not PASS Ratio & Global testing. ###
data <- ProbeQCPassed 

# Check how many unique targets the object has
length(unique(featureData(data)[["TargetName"]]))

# collapse to targets
data <- aggregateCounts(data)



# Update tables containing reasons for probe/segment exclusion:
filtertable_probe$Keep_after_probeQC <-
  rownames(filtertable_probe) %in% unique(fData(data)$TargetName)
filtertable_segment$Keep_after_probeQC <-
  rownames(filtertable_segment) %in% rownames(sData(data))


filtertable_probe$Reason <-
  ifelse(test = is.na(filtertable_probe$Reason),
         yes = ifelse(test = rownames(filtertable_probe) %in% unique(fData(data)$TargetName),
                      yes = NA, no = 'ProbeQC'),
         no = filtertable_probe$Reason)

#### Area and nucleus count QC #####
# We also check if segments are too small to be confident about.

# Histogram of area distribution:
hist(data$area, breaks = 30)
sum(c(data$area < 1000)) # Number of segments with less than 1000 area.


# Change coordinates to numeric in preparation for conversion to
# a spatialexperiment object:
data$x <- as.numeric(data$x)
data$y <- as.numeric(data$y)

# Convert to spatialexperiment object:
spe <- GeomxTools::as.SpatialExperiment(x = data,
                                        normData = 'exprs',
                                        coordinates = c('x', 'y'),
                                        forceRaw = T)

# Save this in case it is needed elsewhere:
save(spe, file = 'IntermediateData/spe_QC.RData')

# Make an expression matrix containing the log counts:
assay(spe, 'logcounts') <- log2(assay(spe))

AlignedReads <- metadata(spe)$sequencingMetrics$Aligned
areaSize <- colData(spe)$area
nucleiCount <- colData(spe)$nuclei
slideName <- colData(spe)$slide_short
segment <- colData(spe)$segment

data2 <-
    cbind(AlignedReads,
          areaSize,
          nucleiCount,
          slideName,
          segment) %>%
as.data.frame()

data2$AlignedReads <- as.numeric(data2$AlignedReads)
data2$areaSize <- as.numeric(data2$areaSize )
data2$nucleiCount <- as.numeric(data2$nucleiCount)

data2$reads_per_area <- data2$AlignedReads/data2$areaSize
data2$flag1 <-
  ifelse(data2$reads_per_area < quantile(data2$reads_per_area)['25%']/2,
         T, F)
data2$reads_per_nuclei


test <- lm(nucleiCount ~ areaSize, data = data2)
summary(test)
predicted_df <- data.frame(nuclei = predict(test, data2), area=data2$areaSize)

### Per slide plots 
data2 %>% 
  ggplot(aes(x = areaSize, y = AlignedReads, color = slideName)) +
  geom_point() + 
  geom_hline(yintercept = 50000) + 
  ggtitle("Aligned reads by Area") + 
  geom_smooth(se = FALSE, color = "black")
# Save the plot:
ggsave(filename = 'Aligned_reads_by_Area_slide.png',
       path = 'QC', device = 'png', width = 20, height = 12, units = 'cm')

data2 %>% 
  ggplot(aes(x = nucleiCount, y = AlignedReads, color = slideName)) +
  geom_point() + 
  geom_hline(yintercept = 20) + 
  ggtitle("Aligned reads by nuclei count") + 
  geom_smooth(se = FALSE, color = "black")
# Save the plot:
ggsave(filename = 'Aligned_reads_by_nuclei_count_slide.png',
       path = 'QC', device = 'png', width = 20, height = 12, units = 'cm')

### Per segment plots
data2 %>% 
  ggplot(aes(x = areaSize, y = AlignedReads, color = segment)) +
  geom_point(mapping = aes(shape = slideName)) + 
  geom_hline(yintercept = 50000) + 
  ggtitle("Aligned reads by Area") + 
  geom_smooth(se = FALSE, color = "black")
# Save the plot:
ggsave(filename = 'Aligned_reads_by_Area_segment.png',
       path = 'QC', device = 'png', width = 20, height = 12, units = 'cm')

data2 %>% 
  ggplot(aes(x = nucleiCount, y = AlignedReads,
             color = segment)) +
  geom_point(mapping = aes( shape = slideName)) + 
  geom_hline(yintercept = 20) + 
  ggtitle("Aligned reads by nuclei count") + 
  geom_smooth(se = FALSE, color = "black")
ggsave(filename = 'Aligned_reads_by_nuclei_count_segment.png',
       path = 'QC', device = 'png', width = 20, height = 12, units = 'cm')

### Area and nuclei count
data2 %>% 
  ggplot(aes(x = areaSize, y = nucleiCount, color = slideName)) +
  geom_point() + 
  geom_hline(yintercept = 20) + 
  ggtitle("nuclei count by area") + 
  geom_smooth(se = FALSE, color = "black")+
  geom_label_repel(data = data2[data2$flag1 == T,],
                   mapping = aes(label = segment),
                   segment.color = 'grey50')
# Save the plot:
ggsave(filename = 'nuclei_count_by_area_slide.png',
       path = 'QC', device = 'png', width = 20, height = 12, units = 'cm')


data2 %>% 
  ggplot(aes(x = areaSize, y = nucleiCount, color = segment)) +
  geom_point() + 
  geom_hline(yintercept = 20) + 
  ggtitle("nuclei count by area") + 
  geom_smooth(se = FALSE, color = "black")
# Save the plot:
ggsave(filename = 'nuclei_count_by_area_segment.png',
       path = 'QC', device = 'png', width = 20, height = 12, units = 'cm')


### PCA
#One initial PCA is always good to check for extreme outlier
#First we'll look at all segments together:

vars <-
    c('slide_short', 'segment_name', 'roi', 's.a._infection_(skin)',
      's.a._infection_(nose)', 'treatment', 'timepoint', 'localization', 'area')


spe_CD3 <- spe[, spe$segment_name == 'CD3+']
spe_CD163 <- spe[, spe$segment_name == 'CD163+']
spe_CD8 <- spe[, spe$segment_name == 'CD8+']


plt <- standR::drawPCA(spe, dims = c(1, 2), assay = 2, color = slide_short)
for(i in vars) {
  j <- sym(i)
  plt$mapping$colour <- j
  plt$labels$colour <- i
  ggsave(plot = plt, filename = paste0('PCA_', i,'.png'), path = 'QC',
         device = 'png', height = 20, width = 22, units = 'cm')
  
}


for(L in unique(spe$segment_name)) {
  subseT <- spe[, spe$segment_name == L]
  plt <- standR::drawPCA(subseT, dims = c(1, 2), assay = 2, color = slide_short)
  for(i in vars) {
    j <- sym(i)
    plt$mapping$colour <- j
    plt$labels$colour <- i
    ggsave(plot = plt, filename = paste0(L, 'PCA_', i,'.png'), path = 'QC',
           device = 'png', height = 20, width = 22, units = 'cm')
    
  }
  
}

#### Final cleanup ####

# Create an intermediate data directory, unless it already exists:
if(!dir.exists('IntermediateData')) {
  dir.create('IntermediateData')
}

# Save the geomx object to disk:
save(file = 'IntermediateData/Data_QC.RData', data) # output path

# Save the filtertables objects to disk:
save(filtertable_probe, file = 'IntermediateData/filtertable_probe.RData')
save(filtertable_segment, file = 'IntermediateData/filtertable_segment.RData')



# Tidy up:
#rm(list=ls())
gc()
