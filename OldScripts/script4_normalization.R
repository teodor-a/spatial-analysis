#~~~~~~~~~~~~~~~~~~~~~~~~~~
###### Libraries used #####
#~~~~~~~~~~~~~~~~~~~~~~~~~~
library(scater)
library(standR)
library(NormalyzerDE)
library(affy)
library(ComplexHeatmap)
library(corrplot)
library(tidyverse)


###### Load data ######
#load(file = "IntermediateData/scaled_spe_03.RData")
load(file = "IntermediateData/non_scaled_spe_03.RData")



if(!dir.exists('Normalization')) {
  dir.create('Normalization')
}


spe <- scater::runPCA(spe, assay.type = "logcounts")

pca_results <- reducedDim(spe, "PCA")






###### PCA ###########
#One initial PCA is always good to check for extreme outlier
#First we'll look at all segments together:

vars <- c('slide_short', 'segment_name', 'roi', 's.a._infection_(skin)',
          's.a._infection_(nose)', 'treatment', 'timepoint', 'localization',
          'area')

#spe_CD3 <- spe[, spe$segment_name == 'CD3+']
#spe_CD163 <- spe[, spe$segment_name == 'CD163+']
#spe_CD8 <- spe[, spe$segment_name == 'CD8+']



plt <- standR::drawPCA(spe, dims = c(1, 2), assay = 2, color = slide_short)
#plt2 <- scater::plotReducedDim(spe, dimred = "PCA", colour_by = "segment") # Alternative PCA plotting function
for(i in vars) {
  j <- sym(i)
  plt$mapping$colour <- j
  plt$labels$colour <- i
  ggsave(plot = plt, filename = paste0(i,'_standR_PCA.png'),
         path = 'Normalization', device = 'png',
         height = 16, width = 20, units = 'cm')
}


for(L in unique(spe$segment_name)) {
  subseT <- spe[, spe$segment_name == L]
  plt <- standR::drawPCA(subseT, dims = c(1, 2), assay = 2, color = slide_short)
  for(i in vars) {
    j <- sym(i)
    plt$mapping$colour <- j
    plt$labels$colour <- i
    ggsave(plot = plt, filename = paste0(L, 'PCA_', i,'.png'),
           path = 'Normalization', device = 'png',
           height = 20, width = 22, units = 'cm')
    
  }
  
}


##### Density and RLE plot, PRE-scaling, pre normalization ##########
mat.CTA <- assay(spe, "logcounts")


pdf('Normalization/raw_density.pdf',
    width = 12, height = 9)
affy::plotDensity(mat = mat.CTA, ylab = 'Density', xlab = 'log2 counts',
                  type = 'l', col = 1:6, main = "raw log2 CTA data",)
dev.off()


scater::plotRLE(spe, assay.type = "logcounts", colour_by  = "slide_short",
                main="INon-normalized, non-scaled counts", exprs_logged = TRUE)
ggsave(filename = 'raw_RLE.png', device = 'png', path = 'Normalization',
       width = 20, height = 13, units = 'cm')


data <- assay(spe, "GeoMx") 

###### Test normalization method: Cyclic Loess ##########


res.CL <- NormalyzerDE::performCyclicLoessNormalization(data)
rownames(res.CL) <- rownames(data)
assay(spe, "cyclic_loess") <- res.CL



spe <- scater::runPCA(spe, assay.type = "cyclic_loess")

pca_results <- reducedDim(spe, "PCA")



plt <- standR::drawPCA(spe, dims = c(1, 2), assay = 3, color = slide_short)
for(i in vars) {
  j <- sym(i)
  plt$mapping$colour <- j
  plt$labels$colour <- i
  ggsave(plot = plt, filename = paste0(i,'_CyclicLoess_standR_PCA.png'),
         path = 'Normalization', device = 'png',
         height = 16, width = 20, units = 'cm')
}


mat.CTA <- assay(spe, "cyclic_loess")
pdf('Normalization/cyclicLoess_density.pdf',
    width = 12, height = 9)
affy::plotDensity(mat = mat.CTA, ylab = 'Density', xlab = 'log2 counts',
                  type = 'l', col = 1:6, main = "raw log2 CTA data",)
dev.off()

scater::plotRLE(spe, assay.type = "cyclic_loess", colour_by  = "slide_short",
                main="Normalized by Cyclic Loess", exprs_logged = TRUE)
ggsave(filename = 'CyclicLoess_RLE.png', device = 'png', path = 'Normalization',
       width = 20, height = 13, units = 'cm')



#### Slides (= patients in our case) still separate on the PCA plots even
#### after normalization, so we need to either analyse patients separately
#### or eliminate this variation during normalization.


######### Some different paths forward ######
## I now want to compare the following data treatments:
# Raw (sort of done above)
# CL-Normalized (sort of done above)
# Scaled by area (hold off for now)
# Batch corrected (three different methods)

## Which data treatment leads to the lowest batch effect?
## (Yes, patient is a batch effect.)


#### Batch correction method : Remove Unwanted Variation 4 (RUV4) ####
# To run batch correction using RUV4, a list of
# “negative control genes (NCGs)” will be required:
spe <- findNCGs(spe, n_assay = 3, batch_name = "slide_short", top_n = 300)
metadata(spe) %>% names()

# Now we run RUV4 using the function geomxBatchCorrection.
# By default this function will use RUV4 to normalize the data.
# We test some different vaues for K:
for(i in seq(5)){
  spe_ruv <- geomxBatchCorrection(spe, method = "RUV4",
                                  n_assay = 3,
                                  factors = "segment_name", 
                                  NCGs = metadata(spe)$NCGs,
                                  k = i)
  # The result gets stored in the "logcounts" assay which is the number 2
  title <- paste0("k = ", i)
  plt <- (plotPairPCA(spe_ruv, assay = 2, n_dimension = 4,
                      color = segment_name, title = title))
  ggsave(plot = plt, filename = paste0('RUV4', title, '.png'), path = 'Normalization')
  
}

# K = 3 seems to be the lowest vaue that still produces separation of the biological groups.

nr <- 3
spe_ruv <- geomxBatchCorrection(spe, method = "RUV4",
                                factors = "segment_name", 
                                n_assay = 3,
                                NCGs = metadata(spe)$NCGs, k = nr)

set.seed(100)

spe_ruv <- scater::runPCA(spe_ruv)

pca_results_ruv <- reducedDim(spe_ruv, "PCA")

plotPairPCA(spe_ruv, assay = 2, precomputed = pca_results_ruv, color = segment_name, title = paste0("RUV4, k = ",nr), n_dimension = 4)
ggsave(filename = 'RUV4_segment_PCA.png', device = 'png', path = 'Normalization')
plotPairPCA(spe_ruv, assay = 2,precomputed = pca_results_ruv, color = slide_short, title = paste0("RUV4, k = ",nr), n_dimension = 4)
ggsave(filename = 'RUV4_slide_PCA.png', device = 'png', path = 'Normalization')

plotRLExpr(spe_ruv, assay = 2, color = slide_short) + ggtitle("RUV4")
ggsave(filename = 'RUV4_slide_RLE.png', device = 'png', path = 'Normalization')

mat.CTA <- assay(spe_ruv, "logcounts")
plotDensity(mat.CTA, main="RUV4", xlab="log expression")





#### Batch correction method: limma ####

spe_lrb <- geomxBatchCorrection(spe,
                                n_assay = 3,
                                batch = colData(spe)$slide_short, method = "Limma",
                                design = model.matrix(~segment_name, data = colData(spe)))
plotPairPCA(spe_lrb, assay = 2, color = segment_name, title = "Limma removeBatch", n_dimension = 4)
ggsave(filename = 'limma_segment_PCA.png', device = 'png', path = 'Normalization')
plotPairPCA(spe_lrb, assay = 2, color = slide_short, title = "Limma removeBatch", n_dimension = 4)
ggsave(filename = 'limma_slide_PCA.png', device = 'png', path = 'Normalization')

plotRLExpr(spe_lrb, assay = 2, color = slide_short) + ggtitle("limma")
ggsave(filename = 'limma_slide_RLE.png', device = 'png', path = 'Normalization')

mat.CTA <- assay(spe_lrb, "logcounts")
plotDensity(mat.CTA, main="limma", xlab="log expression")


#### Batch correction method: RUVg ####
# Just like for the other RUV, need to find best K:
for(i in seq(10)){
  spe_ruvg <- geomxBatchCorrection(spe, method = "RUVg",
                                   n_assay = 3,
                                   factors = "segment_name", 
                                   NCGs = metadata(spe)$NCGs, k = i)
  title <- paste0("k = ", i)
  plt <- plotPairPCA(spe_ruvg, assay = 2, n_dimension = 4,
                      color = segment_name, title = title)
  ggsave(plot = plt, filename = paste0('RUVg', title, '.png'), path = 'Normalization')
}

nr <- 3
spe_ruvg <- geomxBatchCorrection(spe, method = "RUVg",
                                 factors = "segment_name", 
                                 n_assay = 3,
                                 NCGs = metadata(spe)$NCGs, k = nr)

set.seed(100)

spe_ruvg <- scater::runPCA(spe_ruvg)

pca_results_ruvg <- reducedDim(spe_ruvg, "PCA")

plotPairPCA(spe_ruvg, assay = 2, precomputed = pca_results_ruvg, color = segment_name, title = paste0("RUVg, k = ",nr), n_dimension = 4)
ggsave(filename = 'RUVg_segment_PCA.png', device = 'png', path = 'Normalization')

plotPairPCA(spe_ruvg, assay = 2,precomputed = pca_results_ruvg, color = slide_short, title = paste0("RUVg, k = ",nr), n_dimension = 4)
ggsave(filename = 'RUVg_slide_PCA.png', device = 'png', path = 'Normalization')

plotRLExpr(spe_ruvg, assay = 2, color = slide_short) + ggtitle("RUVg")
ggsave(filename = 'RUVg_slide_RLE.png', device = 'png', path = 'Normalization')


mat.CTA <- assay(spe_ruvg, "logcounts")
plotDensity(mat.CTA, main="RUVg", xlab="log expression")




##### Evaluate batch-correction methods ####
## Summary statistics
"
The typical approach to interrogating the effectiveness of batch correction process
on the data uses dimension reduction plots like PCAs.
Here we further suggest the use of summarized statistics to assess the
effectiveness of batch correction.
The 6 summarized statistics tested in this package includes:
  
- Adjusted rand index.
- Jaccard similarity coefficient.
- Silhouette coefficient.
- Chi-squared coefficient.
- Mirkin distance.
- Overlap Coefficient
This assessment can be conducted by using the
plotClusterEvalStats function provided in standR.

Scores for each method will be presented as a barplot scores of the six
summarized statistics (as above) under two sections (biology and batch).
As a general rule, for the biology higher score is considered a good outcome.
On the other hand, for the batch, a smaller score will be the preferred outcome.
"

spe_cyclic_loess <- spe
spe_cyclic_loess <- scater::runPCA(spe_cyclic_loess, assay.type = "cyclic_loess")
pca_results_cyclic_loess <- reducedDim(spe_cyclic_loess, "PCA")

#spe_scaled_cyclic_loess <- spe
#assay(spe_scaled_cyclic_loess, "logcounts") <- scaled_target_data
#spe_scaled_cyclic_loess <- scater::runPCA(spe_scaled_cyclic_loess, assay.type = "logcounts")
#pca_results_scaled_cyclic_loess <- reducedDim(spe_scaled_cyclic_loess, "PCA")

load(file = "IntermediateData/scaled_spe_03.RData")
spe_scaled <- scater::runPCA(spe_cyclic_loess, assay.type = "logcounts")
pca_results_scaled <- reducedDim(spe_scaled, "PCA")


spe_list <- list(spe_cyclic_loess, spe_scaled, spe_ruv, spe_ruvg, spe_lrb)

plotClusterEvalStats(spe_list = spe_list,
                     bio_feature_name = "segment_name",
                     batch_feature_name = "slide_short",
                     data_names = c("CL","Scaling only","CL + RUV4","CL + RUVg","CL + Limma"))
ggsave(filename = 'Evaluations_barplots.png', device = 'png',
       path = 'Normalization', scale = 1.8)


## biplots

plotPCAbiplot(spe_cyclic_loess, n_loadings = 50, assay = "cyclic_loess", 
              precomputed = pca_results_cyclic_loess, color = segment_name,
              title = "cyclic loess")

plotPCAbiplot(spe_scaled, n_loadings = 50, assay = "logcounts", 
              precomputed = pca_results_scaled_cyclic_loess, color = segment_name,
              title = "scaling by Area only")

plotPCAbiplot(spe_ruv, n_loadings = 50, assay = "logcounts", 
              precomputed = pca_results_ruv, color = segment_name,
              title = "CL + RUV4")

plotPCAbiplot(spe_ruvg, n_loadings = 50, assay = "logcounts", 
              precomputed = pca_results_ruv, color = segment_name,
              title = "CL + RUVg")


##### More evaluation of batch-correction methods ####
probelist <- c('CD163','CD68','MRC1','PRF1','CD8A','NKG7','CD4','CD8B','GZMK','CD3D','CD3E')

cd3 <- colData(spe) %>% as.data.frame() %>%
  filter(segment == "CD3+") %>%
  select(dcc, segment)

cd163 <- colData(spe) %>% as.data.frame() %>%
  filter(segment == "CD163+") %>%
  select(dcc, segment)

cd8 <- colData(spe) %>% as.data.frame() %>%
  filter(segment == "CD8+") %>%
  select(dcc, segment)

segments <- rbind(cd3, cd163, cd8) %>%
  mutate(sampleID = rownames(.))

"
#### Heatmap ####
CTA.mat <- assay(spe, 'cyclic_loess')

CTA.mat <- CTA.mat %>% as.data.frame() %>%
  filter(rownames(.) %in% probelist)  %>% t() %>% as.data.frame()

markers <- CTA.mat %>% 
  mutate(dcc = rownames(.)) %>%
  left_join(segments, by = 'dcc') %>%
  pull(segment)

centered_mat <- scale(CTA.mat, center = TRUE, scale = TRUE)
Heatmap(centered_mat, name = 'CL-normd exp', row_split = markers, show_row_names = FALSE)
ggsave(filename = 'heatmap_after_normalization.png', device = 'png')
"



#heatmap function
plot_heatmap <- function(mat, markers, title_name) {
  centered_mat <- scale(mat, center = TRUE, scale = TRUE) #center and scale the expression matrix
  p <- Heatmap(centered_mat, name = title_name, row_split = markers, show_row_names = FALSE)
  
  return(p)
}

#boxplot function
plot_boxplots <- function(mat, probelist, segments) {
  mat <- scale(mat, center = TRUE, scale = TRUE) #center and scale the expression matrix
  centered_mat_long <- mat %>%
    as.data.frame() %>%
    mutate(markers = segments) %>%
    gather(key = "variable", value = "value", -markers) 
  
  p <- centered_mat_long %>%
    ggplot(aes(x=markers, y=value, fill=markers)) +
    geom_boxplot() + 
    theme_bw() +
    theme(legend.position = "none") + 
    facet_wrap(~ variable, scales = "free", labeller = as_labeller(function(probelist) paste("Expression of", probelist))) +
    ylab("Segment")
  
  print(p)
}

#helper function for selecting matrix, marker and genes
get_mat_markers_genes <- function(spe, assay_name, probelist, gene_names = NULL, cluster.res = NULL) {
  CTA.mat <- assay(spe, assay_name) 
  
  CTA.mat <- CTA.mat %>% as.data.frame() %>%
    filter(rownames(.) %in% probelist)  %>% t() %>% as.data.frame() 
  
  markers <- CTA.mat %>% 
    mutate(sampleID = rownames(.)) %>%
    left_join(segments, by = "sampleID") %>%
    pull(segment)
  
  
  return(list("exp.m" = CTA.mat,
              "markers" = markers))
}


plot_corrplot <- function(CTA.mat, markers) {
  #outlizt <- list()
  for (marker in unique(markers)) {
    CTA.seg.mat <- CTA.mat %>% 
      as.data.frame() %>%
      mutate(segment = markers) %>%
      filter(segment == marker) %>%
      select(-segment)
    
    CTA.cor <- cor(CTA.seg.mat, method = "spearman")
    
    p <- corrplot(CTA.cor, method = 'color', 
                  diag = FALSE, type = 'lower', 
                  addCoef.col = 'black',
                  order = 'AOE',
                  title = paste0(marker, " Raw"))
    # outlizt[marker] <- p
    
    print(p$arg$type)
    
  }
 #return(outlizt)
}



#### Heatmaps to evaluate methods ####
matrices <- list(rawcounts = get_mat_markers_genes(spe, "GeoMx", probelist),
                 logcounts = get_mat_markers_genes(spe, "logcounts", probelist),
                 CL = get_mat_markers_genes(spe, "cyclic_loess", probelist),
                 RUV4 = get_mat_markers_genes(spe_ruv, "logcounts", probelist),
                 LIM = get_mat_markers_genes(spe_lrb, "logcounts", probelist),
                 RUVg =  get_mat_markers_genes(spe_ruvg, "logcounts", probelist)
                 )

for(matname in names(matrices)) {
  print(matname)
  
  png(paste0('Normalization/', 'heatmaps_', matname, '.png'), width = 900, height = 800)
  
  plt <- plot_heatmap(matrices[[matname]][['exp.m']],
                      matrices[[matname]][['markers']],
                      matname)
  draw(plt)
  
  dev.off()
}

#### Correlation plots to evaluate methods ####
for(matname in names(matrices)) {
  plt <- plot_corrplot(matrices[[matname]][['exp.m']],
                matrices[[matname]][['markers']])
  
}

#### Boxplots to evaluate methods ####
for(matname in names(matrices)) {
  plt <- plot_boxplots(matrices[[matname]][['exp.m']],
                       probelist,
                       matrices[[matname]][['markers']])
  ggsave(plot = plt, filename = paste0('boxplots_', matname, '.png'),
         path = 'Normalization')

  
}


### Finish up ########
#save(file = "IntermediateData/non_scaled_normalized_spe_04.RData", spe)


##########################################

#res.RLR <- NormalyzerDE::performGlobalRLRNormalization(data)
#res.QN <- NormalyzerDE::performQuantileNormalization(data)
#res.SMAD <- NormalyzerDE::performSMADNormalization(data)
#res.VSN <- NormalyzerDE::performVSNNormalization(data)


#rownames(res.RLR) <- rownames(data)
#rownames(res.QN) <- rownames(data)
#rownames(res.SMAD) <- rownames(data)
#rownames(res.VSN) <- rownames(data)

#assay(spe, "GlobalRLR") <- res.RLR
#assay(spe, "Quantile") <- res.QN
#assay(spe, "SMAD") <- res.SMAD
#assay(spe, "VSN") <- res.VSN




