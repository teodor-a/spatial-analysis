This repository contains the scripts I used to perform the analysis of spatial transcriptomics data as part of my Master thesis in Bioinformatics conducted at Lund University / LTH.

The analysis scripts were originally made by Lavanya Lokhande and Daniel Nilsson, but adapted by me to suit this specific project and questions.

# Background
We want to discern patterns in gene expression in the cutaneous T-cell populations, that may be indicative of incoming progression to advanced CTCL stages.

We have spatially resolved transcriptomic data from two patients, obtained using NanoString GeoMx DSP.

# Dependencies
The script install_packages.R contains a comprehensive list of R packages used.
Please note that the installation of some of the packages requires other
software to first be installed on your system. See the script for details.
# Usage and workflow overview
Run the scripts in the order they are numbered. Scripts depend on the output from the previous script. First install required packages.

There are five scripts. R scripts 1 and 2 perform data import, QC and filtering. From scaling and onward, a standR workflow was used instead of GeoMxTools. So scaling, normalization and batch correction is performed in the R markdown notebooks, numbered 3 and 4. Differential expression analysis is performed in Rmd notebook numbered 5.

Generally, the output from a script, written to a directory named IntermediateData, is used as the starting point in the next script and is loaded in its beginning.
Scripts may also produce output in the form of images and csv files.

## Script 1
This scripts loads the data files including the metadata which is spread out over several excel files. A GeoMx data object is created and quality control is performed. Samples (segments/AOI), and then probes, that do not meet requirements are filtered out.
## Script 2
This script calculates a signal to noise ratio for each probe in each segment, which is used to filter first probes then segments.
## Script 3
This script performs normalisation and batch correction mainly using standR functions with data in SpatialExperiment object.
## Script 4
This script evaluates the results form script 3 using plots of the results.
## Script 5
This script performs differential expression testing mainly using limma-voom. Also explores methods for pathway analysis.
## Make heatmap ROI
This script uses ComplexHeatmap to draw a heatmap of selected markers while also annotating ROI of each segment.
## Figures for report
This script is used to create the figures from the report.

# Notes
Links to some useful tutorials:
- https://bioconductor.org/packages/release/bioc/vignettes/standR/inst/doc/Quick_start.html#data-normalization

Links to some other potentially useful packages:
- https://bioinformatics.mdc-berlin.de/VoltRon/

# TO DO
- [ ] aes_string() is deprecated. Update affected scripts to use tidy evaluation idioms with aes() instead.
- [ ] The dot-dot notation is deprecated. Use after_stat(density) instead.
- [ ] Idea for a cool project: How much technical variation is expected? Find datasets where there is no treatment effect (find such data by finding studies that use only one treatment, or by subsetting multi-treatment data sets), and investigate the variation in them. There should be no variation right? But look at effects of slide, plate, day or lab technician etc. Alternatively, this has already been done and you can look up those studies instead.
- [ ] Change GESECA to analyse baseline and progression separately. This is the only way this methods makes sense in our context.
- [ ] Change the worklfow to utilise writing expression matrices to files (csv or RData) instead of the super confusing method of keeping track of which assay is which. Construct SpatialExperiment objects from scratch at every step if you must, it is probably still less confusing to keep track of.
- [ ] Separate script5 into two scripts; one for DE and a new one for pathway. This will make the reports shorter and gives a better overview.